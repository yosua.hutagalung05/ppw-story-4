from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'home/index.html')

def story1(request):
    return render(request, 'home/story1.html')

def extras(request):
    return render(request, 'home/extras.html')
from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),
    path('story1/', views.story1, name='story1'),
    path('extras/', views.extras, name='extras')
]